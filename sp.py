#!/usr/bin/python3
import paramiko
import os
import configparser
import sys
from time import sleep

#Get and put in a container

from pylxd import Client
client = Client()

config = configparser.ConfigParser()
config.sections()
config.read('scoutplane.ini')

#Some prerequisites copy your default lxc profile, adding the following in cloud config.  And call it 'sp'.
#    runcmd:
#      - [mkdir, /home/ubuntu/workdir, /home/ubuntu/incoming]
#      - [chmod, uog+w, /home/ubuntu/incoming]#

lxdstoragepath = client.storage_pools.get('default').config['source'] + "/containers/"
containerincoming = "/home/ubuntu/incoming/"
insidecont = "/rootfs" + containerincoming

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
try:
	ssh.connect(config['REMOTE']['Server'], username=config['REMOTE']['Username'], password='', key_filename=config['REMOTE']['Key'])
except paramiko.SSHException:
	print("Connection Error")
sftp = ssh.open_sftp()

filepath = sys.argv[1:][0]  #for testing "/home/bryan/1010101/sosreport-cavebox-20180330101241.tar.xz"
filename = os.path.basename(filepath)
dirname = os.path.dirname(filepath).rsplit("/",1)
casenumber = dirname[1]
containername = "sp-" + casenumber

#does a container already exist for this case?
if client.containers.exists(containername):
	print("using existing container: " + containername)
	container = client.containers.get(containername)
	container.start()
else:
	print("creating container: " + containername)
	containerconfig = {'name': containername, 'source': {'type': 'image', "mode": "pull", "server": "https://cloud-images.ubuntu.com/daily", "protocol": "simplestreams", 'alias': 'bionic/amd64'}, 'profiles': ['sp'] }
	container = client.containers.create(containerconfig, wait=True)
	container.start()

sleep(10) #FIXME - this is due AFAICT to the time it takes for cloud-init to start

sendtocont = lxdstoragepath + containername + insidecont + filename
print(sendtocont)
sftp.get(filepath, sendtocont)
sftp.close()
ssh.close()


#If it's likely a sosreport, let's do some analysis.
if filename.startswith('sosreport') and filename.endswith("tar.xz"):
	print(container.execute(['tar','xfJ', containerincoming + filename,'-C','/home/ubuntu/workdir','--exclude=dev/null']),)
	#Add xsos bit here



#Stuff I'm keeping in case it's useful in the future

#Pushing files to lxd
#filedata = open(os.getcwd() + '/runs_on_vm/home.ubuntu.xsos').read()
#container.files.put('/etc/apparmor.d/home.ubuntu.xsos', filedata)

#Now just the container
#addresses = container.state().network['eth0']['addresses']
#for a in addresses:
#	if(a['scope'] == 'global'):
#		address = a['address']
		#print("Found IP [%s]" %(a['address']))
#		break

